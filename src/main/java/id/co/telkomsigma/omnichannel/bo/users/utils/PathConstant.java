/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.utils;

/**
 * @author arief
 *
 */
public final class PathConstant {
	
	public static final String FIND_BY_ID = "/find-by-id";
	public static final String FIND_ALL = "/find-all";
	public static final String SAVE = "/save";
	public static final String UPDATE = "/update";
	public static final String DELETE = "/delete";
	public static final String REGISTER = "/register";
	public static final String LOGIN = "/login";

	public static final String BO_USERS = "/bo-users";
}
