/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.dto;

import java.io.Serializable;

/**
 * @author arief
 *
 */
public class RequestLoginBOUsersDTO implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5045300009221913541L;

	private String email;
	
	private String password;

	public RequestLoginBOUsersDTO() {
		super();
	}

	public RequestLoginBOUsersDTO(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
}
