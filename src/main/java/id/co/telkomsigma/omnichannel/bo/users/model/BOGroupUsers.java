/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author arief
 *
 */
@Entity
@Table(name = "bo_m_group_users")
public class BOGroupUsers extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4949172791183821712L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "uuid_bo_group_user", length = 48, updatable = false)
	private UUID uuid;
	
	@Column(name = "group_name", length = 100)
	private String groupName;

	public BOGroupUsers() {
		super();
	}

	public BOGroupUsers(UUID uuid, String groupName) {
		super();
		this.uuid = uuid;
		this.groupName = groupName;
	}

	public BOGroupUsers(UUID uuid) {
		super();
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
