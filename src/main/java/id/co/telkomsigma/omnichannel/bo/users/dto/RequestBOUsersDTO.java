/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.dto;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author arief
 *
 */
public class RequestBOUsersDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3551993997365936491L;

	private String uuid;
	
	private String uuidGroupUser;
	
	private String fullname;
	
	private String email;
	
	private String phone;
	
	private String password;

	public RequestBOUsersDTO() {
		super();
	}

	public RequestBOUsersDTO(String uuid, String uuidGroupUser, String fullname, String email, String phone, String password) {
		super();
		this.uuid = uuid;
		this.uuidGroupUser = uuidGroupUser;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.password = password;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getUuidGroupUser() {
		return uuidGroupUser;
	}

	public void setUuidGroupUser(String uuidGroupUser) {
		this.uuidGroupUser = uuidGroupUser;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public UUID uuidAsUUID() {
		if (this.uuid != null) {
			if (!(this.uuid.trim().equals(""))) {
				return UUID.fromString(this.uuid);
			}
		}
		
		return null;
	}
	
	public UUID uuidGroupUserAsUUID() {
		if (this.uuidGroupUser != null) {
			if (!(this.uuidGroupUser.trim().equals(""))) {
				return UUID.fromString(this.uuidGroupUser);
			}
		}
		
		return null;
	}
}
