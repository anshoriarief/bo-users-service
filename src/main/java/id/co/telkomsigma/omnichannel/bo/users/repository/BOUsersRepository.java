/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.repository;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.omnichannel.bo.users.model.BOUsers;
import id.co.telkomsigma.omnichannel.bo.users.projections.BOUsersView;

/**
 * @author arief
 *
 */
public interface BOUsersRepository extends JpaRepository<BOUsers, UUID> {

	List<BOUsersView> findAllProjectedBy();
	
	BOUsersView findByUuid(UUID uuid);
	
	BOUsers findByEmail(String email);
	
}
