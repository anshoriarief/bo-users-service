/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * @author arief
 *
 */
@Entity
@Table(name = "bo_m_users")
public class BOUsersDatatableView extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1949200381103190869L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "uuid_bo_m_user", length = 48, updatable = false)
	private UUID uuid;

	@ManyToOne
	@JoinColumn(name = "uuid_bo_group_user")
	private BOGroupUsers uuidGroupUser;

	@Column(name = "fullname", length = 100)
	private String fullname;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "phone", length = 15)
	private String phone;
	
	@Column(name = "isactive")
	private Boolean isActive;

	public BOUsersDatatableView() {
		super();
	}

	public BOUsersDatatableView(UUID uuid, BOGroupUsers uuidGroupUser, String fullname, String email,
			String phone, Boolean isActive) {
		super();
		this.uuid = uuid;
		this.uuidGroupUser = uuidGroupUser;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.isActive = isActive;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public BOGroupUsers getUuidGroupUser() {
		return uuidGroupUser;
	}

	public void setUuidGroupUser(BOGroupUsers uuidGroupUser) {
		this.uuidGroupUser = uuidGroupUser;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
