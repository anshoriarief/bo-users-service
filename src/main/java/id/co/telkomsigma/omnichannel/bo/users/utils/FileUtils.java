/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author arief
 *
 */
public final class FileUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(FileUtils.class);
	
	public static void saveFileToDirectory(String path, MultipartFile multipartFile) throws IOException {
		File file = new File(path);
		if (!file.getParentFile().exists()) {
			LOGGER.info(String.format("Directory %s doesn't exists", file.getParentFile()));
			LOGGER.info("Creating directory...");
			
			file.getParentFile().mkdirs();
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		fos.write(multipartFile.getBytes());
		fos.close();
		
		LOGGER.info(String.format("Successfully save file %s into path %s", multipartFile.getOriginalFilename(), path));
	}

}
