/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import id.co.telkomsigma.omnichannel.bo.users.dto.RequestBOUsersDTO;

/**
 * @author arief
 *
 */
@Entity
@Table(name = "bo_m_users")
public class BOUsers extends BaseModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1949200381103190869L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
	@Column(name = "uuid_bo_m_user", length = 48, updatable = false)
	private UUID uuid;

	@ManyToOne
	@JoinColumn(name = "uuid_bo_group_user")
	private BOGroupUsers uuidGroupUser;

	@Column(name = "fullname", length = 100)
	private String fullname;

	@Column(name = "email", length = 100)
	private String email;

	@Column(name = "phone", length = 15)
	private String phone;

	@Column(name = "password")
	private String password;
	
	@Column(name = "isactive")
	private Boolean isActive;

	public BOUsers() {
		super();
	}

	public BOUsers(UUID uuid, BOGroupUsers uuidGroupUser, String fullname, String email,
			String phone, String password, Boolean isActive) {
		super();
		this.uuid = uuid;
		this.uuidGroupUser = uuidGroupUser;
		this.fullname = fullname;
		this.email = email;
		this.phone = phone;
		this.password = password;
		this.isActive = isActive;
	}

	public BOUsers(RequestBOUsersDTO request, BOGroupUsers groupUsers) {
		super();
		this.uuid = request.uuidAsUUID() != null ? request.uuidAsUUID() : null;
		this.uuidGroupUser = groupUsers;
		this.fullname = request.getFullname();
		this.email = request.getEmail();
		this.phone = request.getPhone();
		this.password = request.getPassword();
		this.isActive = true;
	}

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public BOGroupUsers getUuidGroupUser() {
		return uuidGroupUser;
	}

	public void setUuidGroupUser(BOGroupUsers uuidGroupUser) {
		this.uuidGroupUser = uuidGroupUser;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

}
