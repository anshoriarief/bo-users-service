package id.co.telkomsigma.omnichannel.bo.users.utils;

/**
 * @author Dhamar P
 *
 */
public class Constant {
	
	public class ResponseStatus {
		
		public static final String TRUE = "true";
		public static final String FALSE = "false";
		
	}
	
	public class ResponseMessage {
		
		public static final String SUCCESS = "Success";
		public static final String FAILED = "Oops! Something went wrong";
		public static final String ACCESS_DENIED = "Access Denied";
	}
	
}
