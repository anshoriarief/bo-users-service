/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.co.telkomsigma.omnichannel.bo.users.dto.RequestBOUsersDTO;
import id.co.telkomsigma.omnichannel.bo.users.dto.RequestIdDTO;
import id.co.telkomsigma.omnichannel.bo.users.dto.RequestLoginBOUsersDTO;
import id.co.telkomsigma.omnichannel.bo.users.service.BOUsersService;
import id.co.telkomsigma.omnichannel.bo.users.utils.PathConstant;

/**
 * @author arief
 *
 */
@RestController
@RequestMapping(value = PathConstant.BO_USERS)
public class BOUsersController {

	@Autowired
	private BOUsersService service;
	
	@PostMapping(value = PathConstant.FIND_ALL)
	public ResponseEntity<?> findAll(@RequestBody DataTablesInput request) {
		return service.findAll(request);
	}
	
	@PostMapping(value = PathConstant.FIND_BY_ID)
	public ResponseEntity<?> findById(@RequestBody RequestIdDTO request) {
		return service.findById(request);
	}
	
	@PostMapping(value = PathConstant.SAVE)
	public ResponseEntity<?> save(@RequestBody RequestBOUsersDTO request) {
		return service.save(request);
	}
	
	@PostMapping(value = PathConstant.DELETE)
	public ResponseEntity<?> delete(@RequestBody RequestIdDTO request) {
		return service.delete(request);
	}
	
	@PostMapping(value = PathConstant.LOGIN)
	public ResponseEntity<?> login(@RequestBody RequestLoginBOUsersDTO request) {
		return service.login(request);
	}
}
