/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.datatables.mapping.DataTablesInput;
import org.springframework.data.jpa.datatables.mapping.DataTablesOutput;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.telkomsigma.omnichannel.bo.users.dto.RequestBOUsersDTO;
import id.co.telkomsigma.omnichannel.bo.users.dto.RequestIdDTO;
import id.co.telkomsigma.omnichannel.bo.users.dto.RequestLoginBOUsersDTO;
import id.co.telkomsigma.omnichannel.bo.users.model.BOGroupUsers;
import id.co.telkomsigma.omnichannel.bo.users.model.BOUsers;
import id.co.telkomsigma.omnichannel.bo.users.model.BOUsersDatatableView;
import id.co.telkomsigma.omnichannel.bo.users.projections.BOUsersView;
import id.co.telkomsigma.omnichannel.bo.users.repository.BOGroupUsersRepository;
import id.co.telkomsigma.omnichannel.bo.users.repository.BOUsersRepository;
import id.co.telkomsigma.omnichannel.bo.users.repository.datatable.BOUsersDatatableRepository;
import id.co.telkomsigma.omnichannel.bo.users.utils.Constant;
import id.co.telkomsigma.omnichannel.bo.users.utils.Response;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * @author arief
 *
 */
@Service
public class BOUsersService {

	@Autowired
	private BOUsersRepository repository;
	
	@Autowired
	private BOUsersDatatableRepository datatableRepository;
	
	@Autowired 
	private BOGroupUsersRepository groupUsersRepository;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	public ResponseEntity<?> findAll(DataTablesInput request) {
		try {
			DataTablesOutput<BOUsersDatatableView> datatableBOUsers = datatableRepository.findAll(request);
			
			return new ResponseEntity<>(Response.success(datatableBOUsers), HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	public ResponseEntity<?> findById(RequestIdDTO request) {
		BOUsersView result = null;
		
		if (request.uuidAsUUID() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		
		try {
			result = repository.findByUuid(request.uuidAsUUID());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(Response.success(result), HttpStatus.OK);
	}
	
	@Transactional
	public ResponseEntity<?> save(RequestBOUsersDTO request) {
		try {
			Optional<BOGroupUsers> boGroupUsers = groupUsersRepository.findById(request.uuidGroupUserAsUUID());
			
			if (boGroupUsers.isPresent()) {
				request.setPassword(passwordEncoder.encode(request.getPassword()));
				BOUsers boUser = new BOUsers(request, boGroupUsers.get());
				
				repository.save(boUser);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	public ResponseEntity<?> delete(RequestIdDTO request) {
		try {
			if (request.uuidAsUUID() == null) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
			
			repository.deleteById(request.uuidAsUUID());
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	public ResponseEntity<?> login(RequestLoginBOUsersDTO request) {
		try {
			BOUsers boUser = repository.findByEmail(request.getEmail());			
			
			if (passwordEncoder.matches(request.getPassword(), boUser.getPassword())) {
				Map<String, Object> userMap = new HashMap<String, Object>();
				userMap.put("fullname", boUser.getFullname());
				userMap.put("email", boUser.getEmail());
				userMap.put("phone", boUser.getPhone());
				userMap.put("userGroup", boUser.getUuidGroupUser().getGroupName());
				String jwt = Jwts.builder().setClaims(userMap).signWith(SignatureAlgorithm.HS512, "Balicamp1").compact();
				Map<String, String> jwtMap = new HashMap<String, String>();
				jwtMap.put("jwt", jwt);
				
				return new ResponseEntity<>(Response.success(jwtMap), HttpStatus.OK);
			} else {
				Response response = new Response();
				response.setStatus(Constant.ResponseStatus.FALSE);
				response.setMessage(Constant.ResponseMessage.ACCESS_DENIED);
				return new ResponseEntity<>(response, HttpStatus.UNAUTHORIZED);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
