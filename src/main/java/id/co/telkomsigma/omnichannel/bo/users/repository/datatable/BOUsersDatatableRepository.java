/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.repository.datatable;

import java.util.UUID;

import org.springframework.data.jpa.datatables.repository.DataTablesRepository;

import id.co.telkomsigma.omnichannel.bo.users.model.BOUsersDatatableView;

/**
 * @author arief
 *
 */
public interface BOUsersDatatableRepository extends DataTablesRepository<BOUsersDatatableView, UUID> {
	
}
