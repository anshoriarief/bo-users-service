/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.omnichannel.bo.users.model.BOGroupUsers;

/**
 * @author arief
 *
 */
public interface BOGroupUsersRepository extends JpaRepository<BOGroupUsers, UUID> {

}
