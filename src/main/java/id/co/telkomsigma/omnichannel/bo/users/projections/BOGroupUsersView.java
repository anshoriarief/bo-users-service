/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.projections;

import java.util.UUID;

/**
 * @author arief
 *
 */
public interface BOGroupUsersView {

	UUID getUuid();
	
	String getGroupName();
}
