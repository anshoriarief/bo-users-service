/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.dto;

import java.io.Serializable;
import java.util.UUID;

/**
 * @author arief
 *
 */
public class RequestIdDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3630707754279578407L;

	private String uuid;

	public RequestIdDTO() {
		super();
	}

	public RequestIdDTO(String uuid) {
		super();
		this.uuid = uuid;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public UUID uuidAsUUID() {
		if (this.uuid != null) {
			if (!(this.uuid.trim().equals(""))) {
				return UUID.fromString(this.uuid);
			}
		}
		
		return null;
	}

	@Override
	public String toString() {
		return "RequestIdDTO [uuid=" + uuid + "]";
	}

}
