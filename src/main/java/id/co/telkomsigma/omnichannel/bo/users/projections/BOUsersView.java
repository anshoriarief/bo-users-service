/**
 * 
 */
package id.co.telkomsigma.omnichannel.bo.users.projections;

import java.util.UUID;

/**
 * @author arief
 *
 */
public interface BOUsersView {

	UUID getUuid();
	
	String getFullname();
	
	String getEmail();
	
	String getPhone();
	
	BOGroupUsersView getUuidGroupUser();
}
